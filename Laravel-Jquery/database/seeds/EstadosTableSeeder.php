<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $now = date("Y-m-d H:i:s");
        DB::table("estados")->insert([
            [
                "id" => 11,
                "nome" => "Rondônia",
                "abrv" => "RO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 12,
                "nome" => "Acre",
                "abrv" => "AC",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 13,
                "nome" => "Amazonas",
                "abrv" => "AM",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 14,
                "nome" => "Roraima",
                "abrv" => "RR",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 15,
                "nome" => "Pará",
                "abrv" => "PA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 16,
                "nome" => "Amapá",
                "abrv" => "AP",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 17,
                "nome" => "Tocantins",
                "abrv" => "TO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 21,
                "nome" => "Maranhão",
                "abrv" => "MA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 22,
                "nome" => "Piauí",
                "abrv" => "PI",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 23,
                "nome" => "Ceará",
                "abrv" => "CE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 24,
                "nome" => "Rio Grande do Norte",
                "abrv" => "RN",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 25,
                "nome" => "Paraíba",
                "abrv" => "PB",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 26,
                "nome" => "Pernambuco",
                "abrv" => "PE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 27,
                "nome" => "Alagoas",
                "abrv" => "AL",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 28,
                "nome" => "Sergipe",
                "abrv" => "SE",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 29,
                "nome" => "Bahia",
                "abrv" => "BA",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 31,
                "nome" => "Minas Gerais",
                "abrv" => "MG",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 32,
                "nome" => "Espírito Santo",
                "abrv" => "ES",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 33,
                "nome" => "Rio de Janeiro",
                "abrv" => "RJ",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 35,
                "nome" => "São Paulo",
                "abrv" => "SP",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 41,
                "nome" => "Paraná",
                "abrv" => "PR",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 42,
                "nome" => "Santa Catarina",
                "abrv" => "SC",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 43,
                "nome" => "Rio Grande do Sul",
                "abrv" => "RS",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 50,
                "nome" => "Mato Grosso do Sul",
                "abrv" => "MS",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 51,
                "nome" => "Mato Grosso",
                "abrv" => "MT",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 52,
                "nome" => "Goiás",
                "abrv" => "GO",
                "created_at" => $now,
                "updated_at" => $now,
            ], [
                "id" => 53,
                "nome" => "Distrito Federal",
                "abrv" => "DF",
                "created_at" => $now,
                "updated_at" => $now,
            ],
        ]);
    }

}
