<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/index', 'JqueryController@Index');
Route::get('/estado/{id}', 'JqueryController@Cidades');

Route::get('/cadastrar-jquery', 'JqueryController@CadastrarJquery');
Route::post('/cadastrar-jquery', 'JqueryController@PostCadastrarJquery');

Route::get('/editar-cidade/{id}', 'JqueryController@editCidade');

Route::get('/cidades/{id}', 'JqueryController@comboCidades');

Route::get('/combo', 'JqueryController@comboCidade');

Route::post('/deletar-cidade/{id}', 'JqueryController@deletarCidade');

Route::post('/editar-cidade/{id}', 'JqueryController@editarCidade');

Route::post('/cadastrar-cidade', 'JqueryController@cadastrarCidade');


Route::get('/', function () {
    return view('welcome');
});
