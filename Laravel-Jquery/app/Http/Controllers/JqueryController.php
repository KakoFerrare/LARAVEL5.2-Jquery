<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cadastro;
use App\Estado;
use App\Cidade;

class JqueryController extends Controller {

    private $cadastro, $request, $estado, $cidade;

    public function __construct(Cadastro $cadastro, Estado $estado, Cidade $cidade, Request $request) {
        $this->cadastro = $cadastro;
        $this->estado = $estado;
        $this->cidade = $cidade;
        $this->request = $request;
    }

    public function index() {
        $estados = $this->estado->get();
        return view('jquery.index', compact('estados'));
    }

    public function comboCidade() {
        $estados = $this->estado->orderBy('nome')->get();
        return view('jquery.combo', compact('estados'));
    }

    public function comboCidades($id_estado) {
        $estado = $this->estado->find($id_estado);
        $cidade = $estado->cidade;
        return response()->json($cidade);
    }

    public function cidades($id_estado) {
        $estado = $this->estado->find($id_estado);
        $cidades = $estado->cidade;
        return view('jquery.cidades', compact('cidades', 'estado'));
    }

    public function cadastrarJquery() {
        $titulo = 'Realizar Cadastro';
        return view('jquery.cadastro', compact('titulo'));
    }

    public function postCadastrarJquery() {
        $dadosForm = $this->request->all();
        $validator = validator($dadosForm, $this->cadastro->rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $displayErrors = '';

            foreach ($messages->all("<li>:message</li>") as $message) {
                $displayErrors .= $message;
            }

            return $displayErrors;
        }
        $insert = $this->cadastro->create($dadosForm);
        if ($insert)
            return '1';
        else
            return 'Falha ao Cadastrar';
    }

    public function cadastrarCidade() {
        $dadosForm = $this->request->all();
        $validator = validator($dadosForm, Cidade::$rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $displayErrors = '';

            foreach ($messages->all("<li>:message</li>") as $message) {
                $displayErrors .= $message;
            }

            return $displayErrors;
        }
        $insert = $this->estado->find($dadosForm['id_estado'])->cidade()->create($dadosForm);
        if ($insert)
            return '1';
        else
            return 'Falha ao Cadastrar';
    }

    public function editCidade($id) {
        $cidade = $this->cidade->find($id);
        return response()->json($cidade);
    }

    public function deletarCidade($id) {
        if($this->cidade->find($id)->delete())
            return '1';
        else
            return 'Falha ao Excluir';
    }

    public function editarCidade($id) {
        $dadosForm = $this->request->all();
        $validator = validator($dadosForm, $this->cidade::$rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $displayErrors = '';

            foreach ($messages->all("<li>:message</li>") as $message) {
                $displayErrors .= $message;
            }

            return $displayErrors;
        }
        $update = $this->cidade->find($id)->update($dadosForm);
        if ($update)
            return '1';
        else
            return 'Falha ao Cadastrar';
    }

}
