<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    /**
     * Relationship one to many
     */
    public function cidade(){
        return $this->hasMany('App\Cidade', 'id_estado');
    }
}
