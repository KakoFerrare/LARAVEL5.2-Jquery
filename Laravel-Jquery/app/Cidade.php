<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $fillable = ['id_estado','nome'];
    
    public static $rules = [
        'id_estado'=>'required',
        'nome'=>'required|min:3|max:150'
    ];
}
