@extends('template.template')

@section('content')

<h4> Cidades do Estado <b>{{$estado->nome}}</b></h4>
<a href="{{url('index')}}">Voltar</a>
<br>
<a id="btn-cadastrar" class="waves-effect waves-light btn" href="#modal">Novo</a>
<table class="striped">
    <thead>
        <tr>
            <th>
                Código
            </th>
            <th>
                Nome
            </th>
            <th width="150px">
                Ações
            </th>
        </tr>
    </thead>
    <tbody>
        @forelse( $cidades as $cidade)
        <tr>
            <td>{{$cidade->id}}</td>
            <td>{{$cidade->nome}}</td>
            <td> <a href="#" class="material-icons black-text" onclick="edit('/editar-cidade/{{$cidade->id}}')"> mode_edit </a> |
            <a href="#" class="material-icons black-text" onclick="deletar('/deletar-cidade/{{$cidade->id}}')"> delete</a></td>
        </tr>
        @empty
    <p>Não existem cidades cadastradas para este estado <b>{{$estado->nome}}</b>!</p>
    @endforelse
</tbody>
</table>

<div id="modal" class="modal">
    <div class="modal-content">
        <h4>Gestão Cidades</h4>

        <div class="row">
            <div class="col s12">
                <div class="card-panel red-text text-darken-2 red lighten-5" style="display:none"></div>
            </div>
            <div class="col s12">
                <div class="card-panel green-text text-darken-2 green lighten-5" style="display:none"></div>
            </div>
        </div>
        <div class="row">
            <form class="col s12" method="POST" action="/cadastrar-cidade" id="form" attr-send="/cadastrar-cidade">
                {{csrf_field()}}
                <input type="hidden" name="id_estado" value="{{$estado->id}}">
                <div class="row">
                    <div class="input-field col s12">
                        <input id="nome" name="nome" type="text" class="validate" placeholder="Nome">
                    </div>
                </div>
                <div class="input-field col s12">
                    <div class="progress" style="display: none">
                        <div class="indeterminate"></div>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action modal-close waves-effect waves-red darken-3 btn-flat">Cancelar</button>
            <button type="submit" class="waves-effect waves-green darken-3 btn-flat">Salvar</button>
        </div>
        </form>
    </div>
</div>

<script>
    var urlCadastrar = '/cadastrar-cidade';
    jQuery("#btn-cadastrar").click(function (){
        $("#form").each(function (){
            this.reset();
        });
        jQuery("#form").attr("attr-send",urlCadastrar).attr("action",urlCadastrar);
    });
</script>
@endsection

