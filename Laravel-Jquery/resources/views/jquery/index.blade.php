@extends('template.template')

@section('content')
<h4>Lista de Estados </h4>
<table class="striped">
    <thead>
        <tr>
            <th>
                Id
            </th>
            <th>
                Nome
            </th>
            <th width="100px">
                Abrv
            </th>
            <th width="100px">
                Ações
            </th>
        </tr>
    </thead>
    <tbody>
        @forelse( $estados as $estado)
        <tr>
            <td>{{$estado->id}}</td>
            <td>{{$estado->nome}}</td>
            <td>{{$estado->abrv}}</td>
            <td><a class="material-icons" href="/estado/{{$estado->id}}">toc</a></td>
        </tr>
        @empty
    <p>Não existem cidades cadastradas!</p>

    @endforelse
</tbody>
</table>

@endsection
