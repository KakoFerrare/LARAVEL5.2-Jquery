@extends('template.template')

@section('content')
<div class="row">
    <h3>Cadastro de Cidade</h3>
    <div class="input-field col s12">
        <select id='estado'>
            <option value="" disabled selected>Selecione o estado</option>
            @foreach($estados as $estado)
            <option value="{{$estado->id}}">{{$estado->nome}}</option>
            @endforeach
        </select>
        <label>Estados:</label>
    </div>

    <div class="input-field col s12">
        <select id='cidade' disabled>
            <option value="" disabled selected>Selecione a cidade</option>
        </select>
        <label>Cidades:</label>
    </div>
</div>

<script>
    jQuery("#estado").change(function () {
        var id_estado = jQuery(this).val();
        $('#cidade').empty();
        jQuery.getJSON("/cidades/" + id_estado, function (data) {
            $('#cidade').removeAttr("disabled");
            jQuery.each(data, function (key, value) {
                jQuery("#cidade").append('<option value="' + value.id + '">' + value.nome + '</option>');
            });
            $('select').material_select();
        });
    });
</script>
@endsection