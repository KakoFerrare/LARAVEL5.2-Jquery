@extends('template.template')
@section('content')

<h3> {{$titulo or ''}} </h3>

<div class="row">
    <div class="col s12">
        <div class="card-panel red-text text-darken-2 red lighten-5" style="display:none"></div>
    </div>
    <div class="col s12">
        <div class="card-panel green-text text-darken-2 green lighten-5" style="display:none"></div>
    </div>
</div>
<div class="row">
    <form class="col s12" method="POST" action="/cadastrar-jquery" id="form" attr-send="/cadastrar-jquery">
        {{csrf_field()}}
        <div class="row">
            <div class="input-field col s12">
                <input id="nome" name="nome" type="text" class="validate" placeholder="Nome">
            </div>
            <div class="input-field col s12">
                <input id="email" name="email" type="email" class="validate" placeholder="E-mail">
            </div>
            <div class="input-field col s12">
                <div class="progress" style="display: none">
                    <div class="indeterminate"></div>
                </div>
            </div>
            <div class="input-field col s12">
                <button class="btn waves-effect waves-light" type="submit" name="enviar">Enviar
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </div>
    </form>
</div>

@endsection

