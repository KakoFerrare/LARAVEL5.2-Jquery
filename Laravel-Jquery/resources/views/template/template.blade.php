<!DOCTYPE html>
<html> 
    <head>
        <!--<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->
        <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.css')}}"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{asset('fonts/iconfont/material-icons.css')}}"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta charset="UTF-8">
        <title>{{$titulo or 'Laravel com Jquery e Materialize'}}</title>
        <script type="text/javascript" src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/materialize.js')}}"></script>
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>


        <div id="modalDelete" class="modal">
            <div class="modal-content">
                <h4>Excluir</h4>
                <div class="row">

                    <div class="col s12">
                        <div class="card-panel red-text text-darken-2 red lighten-5" style="display:none"></div>
                    </div>
                    <div class="col s12">
                        <div class="card-panel green-text text-darken-2 green lighten-5" style="display:none"></div>
                    </div>
                    <div class="col s12">
                        <p>Excluir Registro?</p>
                    </div>
                    {{csrf_field()}}
                    <input type="hidden" id="urlDeltar" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="modal-action modal-close waves-effect waves-red darken-3 btn-flat">Cancelar</button>
                    <button type="button" id="btn-confirm-delete" class="waves-effect waves-green darken-3 btn-flat">Deletar</button>
                </div>
            </div>
        </div>
        <script>

$(document).ready(function () {
    $('.modal').modal();
});

$(document).ready(function() {
    $('select').material_select();
  });

$(function () {
    jQuery("#form").submit(function () {
        var dadosForm = jQuery(this).serialize();
        jQuery.ajax({
            url: $(this).attr('attr-send'),
            data: dadosForm,
            method: 'POST',
            beforeSend: startPreloader()
        }).done(function (data) {
            if (data == '1') {
                jQuery("#modal .card-panel").first().hide();
                jQuery("#modal .card-panel").last().html('Cadastrado com sucesso').show();
                setTimeout("location.reload();", 1000)
            } else {
                jQuery("#modal .card-panel").last().hide();
                jQuery("#modal .card-panel").first().html(data).show();
            }
        }).fail(function () {
            alert('Falha ao enviar');
        }).always(function () {
            endPreloader();
        });
        return false;
    });
});

function startPreloader() {
    jQuery(".progress").show();
}

function endPreloader() {
    jQuery(".progress").hide();
}

function edit(urlEditar) {
    jQuery(".modal").modal("open");
    jQuery.getJSON(urlEditar, function (data) {
        jQuery.each(data, function (key, val) {
            jQuery("*[name='" + key + "']").val(val);
        });
    });
    jQuery("#form").attr("attr-send", urlEditar).attr("action", urlEditar);
}

function deletar(urlDeletar) {
    jQuery("#urlDeltar").val(urlDeletar);
    jQuery("#modalDelete").modal("open");
}

jQuery("#btn-confirm-delete").click(function () {
    var urlDelete = jQuery("#urlDeltar").val();
    var csrf = jQuery("#modalDelete input[name='_token']").val();
    jQuery.ajax({
        url: urlDelete,
        method: 'POST',
        data: {'_token': csrf},
        beforeSend: startPreloader()
    }).done(function (data) {
        if (data == '1') {
            jQuery("#modalDelete .card-panel").first().hide();
            jQuery("#modalDelete .card-panel").last().html('Deletado com sucesso').show();
            setTimeout("location.reload();", 3000)
        } else {
            jQuery("#modalDelete .card-panel").last().hide();
            jQuery("#modalDelete .card-panel").first().html(data).show();
        }
    }).fail(function () {
        alert('Falha ao deletar');
    }).always(function () {
        endPreloader();
    });
    return false;
});



        </script>
    </body>
</html> 
